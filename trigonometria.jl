#MAC0110 - MiniEP7
#Gustavo Korzune Gurgel - 4778350

#PARTE 1

function sin(x)
    sen = zero(BigFloat)
    for i = 0:9
        sen += (((-1)^i)*x^(2*i+1))/(factorial(2*i+1))
    end
    return sen
end

function cos(x)
    cos = zero(BigFloat)
    for i = 0:9
        cos += ((-1)^i*x^(2*i))/(factorial(2*i))
    end
    return cos
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
    tangente = zero(BigFloat)
    for i = 1:10
        tangente += (2^(2*i)*(2^(2*i)-1)*bernoulli(i)*x^(2*i-1))/(factorial(2*i))
    end
    return tangente
end

#PARTE 2

function quaseigual(v1,v2) #função para três casas decimais
    δ = 1e-3
    return (abs(big(v1)-big(v2)) < δ)
end

function check_sin(value,x)
    return quaseigual(value, sin(x))
end

function check_cos(value,x)
    return quaseigual(value, cos(x))
end

function check_tan(value,x)
    return quaseigual(value, tan(x))
end

#PARTE 3

function taylor_sin(x)
    sen = zero(BigFloat)
    for i = 0:9
        sen += (((-1)^i)*x^(2*i+1))/(factorial(2*i+1))
    end
    return sen
end

function taylor_cos(x)
    cos = zero(BigFloat)
    for i = 0:9
        cos += ((-1)^i*x^(2*i))/(factorial(2*i))
    end
    return cos
end

function taylor_tan(x)
    tangente = zero(BigFloat)
    for i = 1:10
        tangente += (2^(2*i)*(2^(2*i)-1)*bernoulli(i)*x^(2*i-1))/(factorial(2*i))
    end
    return tangente
end

function test()
   
        #sin
        
    if !check_sin(0, 0)
        println("Algo de errado para sin(x) com x = 0")
    end

    if !check_sin(0.5, pi/6)
        println("Algo de errado para sin(x) com x = π/6")
    end

    if !check_sin((3)^(1/2)/2, pi/3)
        println("Algo de errado para sin(x) com x = π/3")
    end

    if !check_sin(1, pi/2)
        println("Algo de errado para sin(x) com x = π/2")
    end

    if !check_sin(0, pi)
        println("Algo de errado para sin(x) com x = π")
    end

		#cos
    
    if !check_cos(1, 0)
        println("Algo de errado para cos(x) com x = 0")
    end

    if !check_cos((3)^(1/2)/2, pi/6)
       println("Algo de errado para cos(x) com x = π/6")
    end

    if !check_cos(0.5, pi/3)
        println("Algo de errado para cos(x) com x = π/3")
    end

    if !check_cos(0, pi/2)
        println("Algo de errado para cos(x) com x = π/2")
    end
    
    if !check_cos(-1, pi)
        println("Algo de errado para cos(x) com x = π")
    end

		#tan

    if !check_tan(0, 0)
        println("Algo de errado para tan(x) com x = 0")
    end

    if !check_tan((3)^(1/2)/3, pi/6)
        println("Algo de errado para tan(x) com x = π/6")
    end

    if !check_tan(1, pi/4)
        println("Algo de errado para tan(x) com x = π/4")
    end

    if !check_tan((3)^(1/2), pi/3)
        println("Algo de errado para tan(x) com x = π/3")
    end
    
    if !check_tan(0, pi)
        println("Algo de errado para tan(x) com x = π")
    end
end
